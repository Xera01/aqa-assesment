---
# Playwright API Testing Setup Guide
---

This is a Playwright API testing Assesment to Implement a series of actions using the JSONPlaceholder API (https://jsonplaceholder.typicode.com/) to read, create, update, and delete a post, while also verifying the integrity of the operations.

## Features of this framework
* Playwright API Testing

## Getting started

### Pre-requisites
* Download and install Node.js
* Download and install any Text Editor like Visual Code/Sublime/Brackets

### Setup Scripts 
* Clone the repository into a folder
* Go to Project root directory and install Dependency: `npm install`
* All the dependencies from package.json would be installed in node_modules folder.

### Install Visual Code Extension (Optional)
* <a href="https://marketplace.visualstudio.com/items?itemName=ms-playwright.playwright" target="_blank">Playwright Test for VSCode</a>

### Update Visual Code Settings
* Go to Visual Code Preference > Setting and search `formatOnSave` and enable/ON it.

## How to Run Test Locally
* Go to the Project root directory and run command: `npm test`

## How to Run Single Spec Locally
* Go to the Project root directory and run command: `npx playwright test tests/01_send_A_get_request_to_post.spec.js`

## How to view default Playwright HTML report
* Go to the Project root directory and run command: `npx playwright show-report`

### Playwright HTML Test Report
![Playwright HTML Test Report](./assets/html-test-report.PNG?raw=true "Playwright HTML Test Report")