// @ts-check
const { test, expect } = require('@playwright/test');

//Verify that the post has been successfully deleted by attempting to retrieve it using a GET request to /posts/{id} and ensuring a 404 status code is returned.


test('should be able to delete newly created posts', async ({ request }) => {
//Send a DELETE request to /posts/{id} to delete the post, replacing {id} with the ID of the created post.
const response = await request.delete('/posts/101');
console.log(await response.json());
//console.warn(xhr.responseText);
expect(response.ok()).toBeTruthy();
expect(response.status()).toBe(200);
});

//Verify that the post has been successfully deleted by attempting to retrieve it using a GET request to /posts/{id} and ensuring a 404 status code is returned.
test('should be able to Verify that the post has been successfully deleted', async ({ request }) => {
    const response = await request.get('/posts/101');
    console.log(await response.json());
    //console.warn(xhr.responseText);
    expect(response.ok()).toBeFalsy();
    expect(response.status()).toBe(404);
    });