const {test, expect} = require('@playwright/test');
import { faker } from '@faker-js/faker';
const { DateTime } = require("luxon");



test('test should be able to create a new post', async ({request}) => {
    //Send a POST request to /posts with a new post object containing relevant data.
    const response = await request.post("/posts",{
        data:  {
            "title": 'New Post',
            "body": 'This is a new post created using Playwright and JSONPlaceholder API',
            "userId": 1 // Replace with the desired user ID
        }
       
    });
      
    console.log(await response.json());
    expect(response.ok()).toBeTruthy();
    expect(response.status()).toBe(201);
    const responseBody = await response.json()
    //Extract the ID of the newly created post from the response body.
    //Store the ID of the created post in a variable for future reference.
    const responsebodyID = responseBody.id;
    console.log('Newly created post userID:', responsebodyID);
    //expect(responseBody.id).toHaveProperty("id", responsebodyID);
    //expect(responseBody.userId).toHaveProperty("userId", userId);
});


