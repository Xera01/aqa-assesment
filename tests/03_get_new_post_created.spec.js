// @ts-check
const { test, expect } = require('@playwright/test');




test('should be able to get the newly created posts', async ({ request }) => {
//Send a GET request to /posts/{id} endpoint, replacing {id} with the ID of the created post.
const response = await request.get('/posts/101');

//Retrieve and validate the details of the created post from the response body.
console.log(await response.json());
//console.warn(xhr.responseText);
expect(response.ok()).toBeTruthy();
expect(response.status()).toBe(200);
});