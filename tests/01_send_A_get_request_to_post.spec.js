// @ts-check
const { test, expect } = require('@playwright/test');


test('should be able to retrieve the total number of posts', async ({ request }) => {
//Send a GET request to /posts.
var response = await request.get('/posts');
const posts = await response.json();
//Store the total number of posts in a variable for future reference
const totalPosts = posts.length;
//Retrieve the total number of posts from the response body.
console.log('Total number of posts:', totalPosts);
expect(response.ok()).toBeTruthy();
expect(response.status()).toBe(200);
});