const {test, expect} = require('@playwright/test');
import { faker } from '@faker-js/faker';
const { DateTime } = require("luxon");


test('test should be able to update update fields', async ({request}) => {
    //Send a PATCH request to /posts/{id} with updated field(s) of the post, replacing {id} with the ID of the created post.

    const response = await request.patch("/posts/101",{
        data:  {
            "title": 'New Pacth',
            "body": 'This is a new post created using Playwright and JSONPlaceholder API',
            "userId": 11 // Replace with the desired user ID
        }
       
    });
      
    console.log(await response.json());
    expect(response.ok()).toBeTruthy();
    expect(response.status()).toBe(200);
    const responseBody = await response.json()
    const responsebodyID = responseBody.userId;
    console.log('Newly created post ID:', responsebodyID);
    console.log('Newly created post:', responseBody);
    //expect(responseBody.id).toHaveProperty("id", responsebodyID);
    //expect(responseBody.userId).toHaveProperty("userId", userId);
});


//Confirm the successful update of the post by sending a GET request to /posts/{id} and verifying the changes.
test('should be able to get the newly created posts', async ({ request }) => {
    //Send a GET request to /posts/{id} endpoint, replacing {id} with the ID of the created post.
    const response = await request.get('/posts/101');
    
    //Retrieve and validate the details of the updated post from the response body.
    console.log(await response.json());
    //console.warn(xhr.responseText);
    expect(response.ok()).toBeTruthy();
    expect(response.status()).toBe(200);
    });